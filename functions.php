<?php

use mgmsr\lib\admin\Options;
use mgmsr\lib\Results;

/**
 * for wp-cron
 */
add_action(MGMSR_CRON_NAME, 'search_replace');
function search_replace(){
	fppr(time(), __FILE__ . ' search_replace time()');
	$search = Options::get_option('search');
	$replace = Options::get_option('replace');
	$where = Options::get_option('where');
	$where = is_array($where) ? $where : array();
	$results = Results::get_items(array(
		"search" => $search,
		"replace" => $replace,
		"where" => $where,
	));
	fppr($results, __FILE__ . ' search_replace Results::get_items');
	
	$results = Results::replace($results);
	fppr($results, __FILE__ . ' search_replace Results::replace');
}

/**
 * (RA) Helpers
 */
if (!function_exists("ppr")) {
    function ppr($_arr, $_ar_name)
    {
        echo "<pre>$_ar_name<br>";
        print_r($_arr);
        echo "</pre>";
    }
}
if (!function_exists("fppr")) {
    function fppr($_arr, $_ar_name, $_log_name = false, $append = FILE_APPEND)
    {
        $_log_name = ($_log_name) ? $_log_name : "rlog";
        $str = date("d.m.y H:m:s") . "\r\n" . $_ar_name . "\r\n" . print_r($_arr, true) . "\r\n-------------------\r\n";
        file_put_contents(MGMSR_PLUGIN_DIR . $_log_name . '.txt', $str, $append);
    }
}

if (!function_exists("jsalert")) {
    function jsalert($mess = "jsalert: Hello!")
    {
        //header( 'Content-Type: text/html; charset=utf-8' );
        echo '<script type="text/javascript">alert("' . $mess . '");</script>';
    }
}