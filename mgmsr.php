<?php

/* ====================================
 * Plugin Name: MGM Search & Replace
 * Description: Search and Replace all strings in Pages/Blogs
 * Author: Abdilkhamidov R.
 * Version: 1.0.0
 * ==================================== */

namespace mgmsr;
use mgmsr\lib\Base;

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
//
define('MGMSR_PLUGIN_NAME', __("MGM Search & Replace", "mgmsr"));
define('MGMSR_PLUGIN_DESCRIPTION', __("Search and Replace all strings in Pages/Blogs. Be carefully and sure what you do because replacing part(s) of WordPress code may crash found posts!", "mgmsr"));

define('MGMSR_PLUGIN_CODE', "mgmsr");
define('MGMSR_ADMINMENU_SLUG', "mgmsr");
define('MGMSR_PLUGIN_ABBR', "mgmsr");
define('MGMSR_PLUGIN_VERSION', '1.0.0');
//define('MGMSR_UPLOAD_DIR', 'mgmsr/');

define('MGMSR_PLUGIN_DIR', plugin_dir_path(__FILE__) );
define('MGMSR_PLUGIN_URL', plugin_dir_url(__FILE__));
define('MGMSR_FRONT_TEMPLATES_DIR', MGMSR_PLUGIN_DIR . 'templates/front/' );
define('MGMSR_FRONT_TEMPLATES_URL', MGMSR_PLUGIN_URL . 'templates/front/' );
define('MGMSR_ADMIN_TEMPLATES_DIR', MGMSR_PLUGIN_DIR . 'templates/admin/' );
define('MGMSR_ADMIN_TEMPLATES_URL', MGMSR_PLUGIN_URL . 'templates/admin/' );
define('MGMSR_SHORTCODE_TEMPLATES_DIR', MGMSR_PLUGIN_DIR . 'templates/shortcodes/' );
define('MGMSR_SHORTCODE_TEMPLATES_URL', MGMSR_PLUGIN_URL . 'templates/shortcodes/' );

define('MGMSR_CRON_NAME', 'mgmsr_cron_search_replace' );

if(!defined("MGMSR_SITE_URL")){
    define('MGMSR_SITE_URL', (is_ssl() ? "https://" : "http://") . $_SERVER["SERVER_NAME"]);
}

require_once MGMSR_PLUGIN_DIR . "functions.php";

global $mgmsr;

if (!class_exists('Mgmsr')) {
    require_once plugin_dir_path(__FILE__) . 'lib/Psr4AutoloaderClass.php';
    $loader = new lib\Psr4AutoloaderClass();
    $loader->register();
    $loader->addNamespace(__NAMESPACE__, untrailingslashit(plugin_dir_path(__FILE__)));
    
    class Mgmsr extends Base{
        const VERSION = MGMSR_PLUGIN_VERSION;
    
        //public function activate(){
        //    $this->create_upload_dirs();
        //}
        //
        //public function deactivate(){
        //}
        
        public static function version() {
            return self::VERSION;
        }
    }
	$mgmsr = new Mgmsr();
} else {
    die('Plugin ' . MGMSR_PLUGIN_CODE .' is already activated.');
}

register_activation_hook( __FILE__, array($mgmsr, 'activate'));
register_deactivation_hook( __FILE__, array($mgmsr, 'deactivate'));