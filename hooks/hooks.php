<?php
/**
 * some hooks
 */

// to disable jquery-migrate.js console tracing
add_action( 'wp_default_scripts', 'mgmsr_remove_jquery_migrate' );
function mgmsr_remove_jquery_migrate( $scripts ) {
    if ( isset( $scripts->registered['jquery'] ) ) {
        $script = $scripts->registered['jquery'];
        if ( $script->deps ) {
            $script->deps = array_diff( $script->deps, array( 'jquery-migrate' ) );
        }
    }
}
