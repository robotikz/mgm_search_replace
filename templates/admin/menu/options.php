<?php defined( 'ABSPATH' ) || exit; ?>
<?php
use mgmsr\lib\admin\Options;
?>
<div class="mgmsr-options mgmsr-nav-tab">
    <form class="mgmsr-admin-form" id="mgmsr-options-form" method="post" autocomplete="off">
        <h3><?php _e("Options", "mgmsr"); ?></h3>
        <table class="form-table">
            <tr>
                <th scope="row"><label for="mgmsr-option"><?php _e("From", "mgmsr");?></label></th>
                <td>
                    <input name="option" type="text" id="mgmsr-option" value="<?php echo Options::get_option('option'); ?>" class="regular-text" required="required" />
                    <p class="description">
                    </p>
                </td>
            </tr>
        </table>
        <input type="hidden" name="mgmsr_action_update" value="1" />
        <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('mgmsr_action_update'); ?>" />
        <small>* - <?php _e("required fields", "mgmsr"); ?></small>
        <?php submit_button(); ?>
    </form>
</div>