<?php defined('ABSPATH') || exit;?>
<?php
use mgmsr\lib\admin\Options;

$search = !empty($_POST["search"]) ? sanitize_text_field($_POST["search"]) : Options::get_option('search');
$replace = !empty($_POST["replace"]) ? sanitize_text_field($_POST["replace"]) : Options::get_option('replace');
$where = !empty($_POST["where"]) ? $_POST["where"] : Options::get_option('where');
$where = is_array($where) ? $where : array();
?>
<div class="mgmsr-options mgmsr-nav-tab">
    <form class="mgmsr-admin-form" id="mgmsr-options-form" method="post" autocomplete="off">
        <div data-role="tab" data-tab="settings">
            <h3><?php _e("Search & Replace", "mgmsr");?></h3>
            <table class="form-table">
                <tr>
                    <th scope="row"><label for="mgmsr-search"><?php _e("From", "mgmsr");?>*</label></th>
                    <td>
                        <input name="search" type="text" id="mgmsr-search" value="<?php echo $search; ?>" class="regular-text" required="required" />
                        <p class="description"><?php _e("Case sensitive", "mgmsr");?></p>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="mgmsr-replace"><?php _e("To", "mgmsr");?>*</label></th>
                    <td>
                        <input name="replace" type="text" id="mgmsr-replace" value="<?php echo $replace; ?>" class="regular-text" required="required" />
                        <p class="description"><?php _e("Case sensitive", "mgmsr");?></p>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="mgmsr_where"><?php _e("Where to replace", "mgmsr");?>*</label></th>
                    <td>
                        <input name="where[]" type="checkbox" id="mgmsr-where-blogs" class="mgmsr-where-blogs" value="post" <?php checked(in_array("post", $where), true);?>>
                        <label for="mgmsr-where-blogs"><?php esc_html_e('Blogs', 'mgmsr');?></label><br>

                        <input name="where[]" type="checkbox" id="mgmsr-where-pages" class="mgmsr-where-pages" value="page" <?php checked(in_array("page", $where), true);?>>
                        <label for="mgmsr-where-pages"><?php esc_html_e('Pages', 'mgmsr');?></label>

                        <p class="description">
                        </p>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="mgmsr-cron"><?php _e("WP-Cron Settings (empty nothing happens)", "mgmsr");?></label></th>
                    <td>
                        <input name="cron" type="text" id="mgmsr-cron" value="<?php echo Options::get_option('cron'); ?>" class="regular-text" />
                        <p class="description"><?php _e("Start with: hourly or daily", "mgmsr");?></p>
                        <p class="description"><?php _e("Empty string to deactivate", "mgmsr");?></p>
                        <p class="description">
	                        <?php _e("See", "mgmsr");?>: <a href="https://developer.wordpress.org/plugins/cron/understanding-wp-cron-scheduling/">Understanding WP-Cron Scheduling</a>
                        </p>
                    </td>
                </tr>
            </table>
        </div>
        <input type="hidden" name="mgmsr_action_update" value="1" />
        <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('mgmsr_action_update'); ?>" />
        <small>* - <?php _e("required fields", "mgmsr");?></small>
        <p>
            <button type="submit" name="mgmsr_save_btn" class="mgmsr-save-btn button button-primary" value="1"><?php _e("Save", "mgmsr");?></button>
        </p>
        <br>
        <h3><?php _e("Start manually", "mgmsr");?></h3>
        <div>
            <button type="submit" name="mgmsr_search_show_btn" class="mgmsr-search-show-btn button button-primary" value="1"><?php _e("Search & Show", "mgmsr");?></button>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <button type="submit" name="mgmsr_search_replace_btn" class="mgmsr-search-replace-btn button button-link-delete" value="1"><?php _e("Search & Replace", "mgmsr");?></button>
        </div>
        <div class="preloader-overlay" id="plo_mgmsr_form"><div class="preloader"></div></div>
    </form>
</div>

<?php
global $mgmsr_admin;
$mgmsr_admin->display_results_table();
?>