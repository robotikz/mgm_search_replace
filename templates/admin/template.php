<?php defined( 'ABSPATH' ) || exit; ?>

<div class="wrap">
    
    <h2><?php _e(MGMSR_PLUGIN_NAME, "mgmsr"); ?></h2>
    <p><?php _e(MGMSR_PLUGIN_DESCRIPTION, "mgmsr"); ?></p>
    <?php echo !empty($nav) ? $nav : ""; ?>
    
    <?php if(isset($notices)): foreach ($notices as $key => $notice): ?>
        <?php if($notice): ?>
            <div id="message" class="notice notice-<?php echo $key ?> is-dismissible">
                <p><?=implode(". ", $notice); ?></p>
            </div>
        <?php endif; ?>
    <?php endforeach; endif; ?>
    
    <br />
    <?php echo isset($content) ? $content : ""; ?>
</div>