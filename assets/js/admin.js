/*
 * Scripts for admin
 */

(function ($) {
	"use strict";
	const {
		__
	} = wp.i18n;
	// var locale = document.documentElement.lang;
	var global = {
		"locale": document.documentElement.lang,
	}

	$(document).ready(function () {
		//console.log("mgmsr_admin_params", mgmsr_admin_params); // params from Admin->action_enqueue_assets()
		if (mgmsr_admin_params.intl_locales !== undefined && mgmsr_admin_params.intl_locales !== '') {
			global.locale = mgmsr_admin_params.intl_locales;
		}
	})

	/**
	 * Search-show
	 */
	.on('click', '.mgmsr-search-show-btn', function (e) {
		var form = $(this).closest('form#mgmsr-options-form');
		var preloader = $(form).find('.preloader-overlay#plo_mgmsr_form');
		var form_data = new FormData($(form)[0]);
		// check if required field are filled
		if(form_data.get("search") && form_data.get("replace")){
			$(preloader).fadeIn();
		}
	})

	/**
	 * Search-replace
	 */
	.on('click', '.mgmsr-search-replace-btn', function (e) {
		var form = $(this).closest('form#mgmsr-options-form');
		var form_data = new FormData($(form)[0]);
		var preloader = $(form).find('.preloader-overlay#plo_mgmsr_form');
		let is_replace = confirm(__("Are sure to search & replace?", "mgmsr"));
		if(!is_replace){
			e.preventDefault();
		}else{
			// check if required field are filled
			if(form_data.get("search") && form_data.get("replace") && form_data.get("where")){
				$(preloader).fadeIn();
			}
		}
	})
	;

	//$(window).on('load', function () {});

	/*
	 * functions
	 */

})(jQuery);
