<?php
namespace mgmsr\lib;

//use mgmsr\lib\db\Db_search_queries;

class Base
{
    public function __construct()
    {
        $this->params = array();
        $this->results = array();
        require_once MGMSR_PLUGIN_DIR . "hooks/hooks.php";
        add_action('init', array($this, 'action_init'));
    }

    public function activate()
    {
        //Db_search_queries::create_table();

    }

    public function deactivate()
    {
        wp_clear_scheduled_hook(MGMSR_CRON_NAME);
    }

    public function action_init()
    {
        load_textdomain("mgmsr", MGMSR_PLUGIN_DIR . 'languages/' . MGMSR_PLUGIN_CODE . "-" . determine_locale() . '.mo');

        if (is_admin()) {
            global $qrcovid_admin;
            $qrcovid_admin = new admin\Admin();
        }else{
	        new front\Wc();
	        new front\Wc_product_page();
        }
    }
}
