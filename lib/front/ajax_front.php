<?php
namespace mgmsr\lib\front;

/**
 * Ajax functions for front
 */
class Ajax_front
{
	public function __construct(){
		if( wp_doing_ajax() ){
			add_action('wp_ajax_request_name', array($this, 'request_name'));
			add_action('wp_ajax_nopriv_request_name', array($this, 'request_name'));
		}
	}
	
	/**
	 *
	 */
	public function request_name(){
		//fppr($_POST, __FILE__.' $_POST');
		try{
			$response = array("request" => $_POST, "errors" => array(), "success" => false);
			if ( empty($_POST) || ! wp_verify_nonce( $_POST['_mgmsr_editsvg_nonce'], 'mgmsr_editsvg_nonce') ){
				throw new \Exception(__("Error checking security code", "mgmsr"));
			}
			if(empty($_POST["attachment_id"])){
				throw new \Exception(__("Attachment id of pdf template is not recevied", "mgmsr"));
			}
			
			// all ok!
			
			$response["success"] = true;
			echo json_encode($response);
			wp_die();
			
		} catch (\Throwable $e) {
			$this->result["errors"][] = $e->getMessage();
			echo json_encode($response); wp_die();
		}
	}

}
