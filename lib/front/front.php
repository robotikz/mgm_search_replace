<?php
namespace mgmsr\lib\front;

use mgmsr\lib\Base;
use mgmsr\lib\admin\Options;

class Front extends Base
{
    public static $css_version = "1.0.0";
    public static $js_version = "1.0.0";

	public function __construct(){
		parent::__construct();
        add_action( 'wp_enqueue_scripts', array($this, 'action_enqueue_assets'));
	}
	
	/**
	 * Enqueue styles and scripts
	 */
	public function action_enqueue_assets(){
        wp_enqueue_style(MGMSR_PLUGIN_CODE,  MGMSR_PLUGIN_URL . 'assets/css/style.min.css', array(), self::$css_version, 'all');
        
        //wp_enqueue_style("fontawesome-all",  'https://use.fontawesome.com/releases/v5.15.3/css/all.css', array(), "5.15.3", 'all');
        
        wp_enqueue_script(MGMSR_PLUGIN_CODE . '-main', MGMSR_PLUGIN_URL . 'assets/js/front.js', array(
            'jquery', 'wp-i18n'), self::$js_version, true);
        wp_localize_script(MGMSR_PLUGIN_CODE . '-main', MGMSR_PLUGIN_ABBR . '_params', array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'intl_locales' => Options::get_option( 'intl_locales' ),
            //'trans' => array(
            //    'Challenge someone' => __('Challenge someone', 'courtres'),
            //)
        ));
	}
 
}
