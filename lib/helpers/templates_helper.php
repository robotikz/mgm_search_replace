<?php

namespace mgmsr\lib\helpers;

class Templates_helper {

    public static function load($filename, $data = array()) {
        ob_start();
        extract($data, EXTR_SKIP);
        include $filename;
        return ob_get_clean();
    }
}
