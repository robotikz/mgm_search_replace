<?php

namespace mgmsr\lib\db;

class Db_search_queries extends Db_base {
    
	public static $table_name = MGMSR_PLUGIN_ABBR . "_search_queries";

	static function get_db_fields(){
		$db_fields = array(
			"id" => array(
				"code" => "id",
				"title" => "id",
				"show_in_admin" => true,
				"default_value" => false
			),
            "search" => array(
                "code" => "search",
                "title" => __("Search for", "mgmsr"),
                "show_in_admin" => true,
                "default_value" => false
            ),
            "replace" => array(
                "code" => "replace",
                "title" => __("Replace to", "mgmsr"),
                "show_in_admin" => true,
                "default_value" => false
            ),
            "where" => array(
                "code" => "where",
                "title" => __("Where to replace", "mgmsr"), // may be "blogs", "pages"
                "show_in_admin" => true,
                "default_value" => false
            ),
			"created_dt" => array(
				"code" => "created_dt",
				"title" => __("Created date and time", "courtres"),
				"show_in_admin" => true,
				"default_value" => false
			),
			"modified_dt" => array(
				"code" => "modified_dt",
				"title" => __("Modified date", "courtres"),
				"show_in_admin" => true,
				"default_value" =>  false
			),
		);
		return $db_fields;
	}

	
	static function create_table(){
		global $wpdb;
		$db_fields = self::get_db_fields();
		$sql = sprintf(
			"CREATE TABLE IF NOT EXISTS `%1\$s` (
				`id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
				`search` varchar(526),
				`replace` varchar(526),
				`where` varchar(128),
				`is_active` boolean DEFAULT 1,
				`created_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
				`modified_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
				) %2\$s AUTO_INCREMENT=1;",
				
				self::get_table_name(),
				self::get_charset_collate()
		);
		$wpdb->query( $sql );
	}
 
}
