<?php

namespace mgmsr\lib;

class Base_options
{
    static $options = array(
        "search" => array(
            "code" => "search",
            "default" => "",
	        "type" => "string"
        ),
        "replace" => array(
            "code" => "replace",
            "default" => "",
            "type" => "string"
        ),
        "where" => array(
            "code" => "where",
            "default" => "", // may be "blogs", "pages"
            "type" => "array"
        ),
        "cron" => array(
            "code" => "cron",
            "default" => "", //See: https://en.wikipedia.org/wiki/Cron
            "type" => "string"
        ),
    );
    static $gtm_format = 'Y-m-d H:i:s';

    public function __construct()
    {
    }
	
    public static function get_option($option)
    {
        if (static::is_valid_option($option)) {
	        $value = get_option(sprintf('%s_%s', MGMSR_PLUGIN_ABBR, $option), static::$options[$option]["default"]);
	        if(static::$options[$option]["type"] == "array"){
		        $value = unserialize($value);
	        }
	        return $value;
        } else {
            return false;
        }
    }

    public static function update_option($option, $value)
    {
        if (static::is_valid_option($option)) {
            return update_option(sprintf('%s_%s', MGMSR_PLUGIN_ABBR, $option), $value);
        } else {
            return false;
        }
    }

    public static function is_valid_option($option)
    {
        //return key_exists($option, static::$defaults);
        return true;
    }

    //public function get_all(){
    //    return $this->params;
    //}
}
