<?php

namespace mgmsr\lib\admin;

class cron {

	public function __construct(){}
	
	public function schedule($recurrence)
	{
		if (!empty($recurrence)) {
			if (!wp_next_scheduled(MGMSR_CRON_NAME)) {
				wp_schedule_event(time(), $recurrence, MGMSR_CRON_NAME);
			}
			if (!wp_next_scheduled('my_hourly_event')) {
				wp_schedule_event(current_time('timestamp'), 'hourly', 'my_hourly_event');
			}
		} else {
			$this->unschedule();
		}
	}
	
	public function unschedule()
	{
		// $timestamp = wp_next_scheduled(MGMSR_CRON_NAME);
		// wp_unschedule_event($timestamp, MGMSR_CRON_NAME);
		wp_clear_scheduled_hook(MGMSR_CRON_NAME);
	}
}