<?php

namespace mgmsr\lib\admin;

use mgmsr\lib\Base_options;

class Options extends Base_options
{

    public function __construct()
    {
        parent::__construct();
        add_action('admin_init', array($this, 'action_init'));
        //add_action('admin_head', array($this, 'action_admin_head'));
    }

    public function action_init()
    {
        // update options
        if (isset($_POST['mgmsr_action_update']) && isset($_POST['nonce']) && is_admin()) {
            if (wp_verify_nonce($_POST['nonce'], 'mgmsr_action_update')) {
                $_POST["where"] = empty($_POST["where"]) ? array() : $_POST["where"];
                foreach ($_POST as $key => $value) {
                    if (!empty($value) && is_array($value)) {
                        $value = serialize($value);
                    }
                    $this->update_option($key, wp_unslash(sanitize_text_field($value)));
                }
                // schedule cron
	            $cron = new Cron();
	            if(!empty($_POST["cron"])){
		            $cron->schedule($_POST["cron"]);
	            }else{
		            $cron->unschedule();
	            }
            }
        }
    }
    
}

