<?php
namespace mgmsr\lib\admin;

use mgmsr\lib\admin\Options;
use mgmsr\lib\Base;
use mgmsr\lib\helpers\Templates_helper;
use mgmsr\lib\Results;

class Admin extends Base
{
    static $css_version = "1.0.0";
    static $js_version = "1.0.0";
    public $options;

    public function __construct()
    {
        parent::__construct();
        add_action('admin_enqueue_scripts', array($this, 'action_enqueue_assets'));
        add_action('admin_menu', array($this, 'action_admin_menu'), 20);
	
        $this->options = new Options;
    }

    /**
     * Register the stylesheets for the admin area.
     */
    public function action_enqueue_assets()
    {
        wp_enqueue_style(MGMSR_PLUGIN_CODE . '-admin', MGMSR_PLUGIN_URL . 'assets/css/admin-style.min.css', array(), self::$css_version);
        //
        wp_enqueue_script(MGMSR_PLUGIN_CODE . '-admin', MGMSR_PLUGIN_URL . 'assets/js/admin.js', array('jquery', 'wp-i18n'), self::$js_version, false);
        wp_localize_script(MGMSR_PLUGIN_CODE . '-admin', MGMSR_PLUGIN_ABBR . '_admin_params', array(
            'intl_locales' => get_bloginfo("language"),
        ));
    }

    public function action_admin_menu()
    {
        add_menu_page(
            ucfirst(MGMSR_PLUGIN_CODE),
            MGMSR_PLUGIN_NAME,
            'manage_options',
            sprintf('%s-sr', MGMSR_PLUGIN_ABBR),
            array($this, 'display_sr_page'),
            'dashicons-list-view', "80.5");

        //add_submenu_page(
        //    MGMSR_ADMINMENU_SLUG,
        //    __('Options', 'mgmsr'),
        //    __('Options', 'mgmsr'),
        //    'manage_options',
        //    sprintf('%s-options', MGMSR_PLUGIN_ABBR),
        //    array($this, 'display_options_page')
        //);

        //add_submenu_page(
        //    MGMSR_ADMINMENU_SLUG,
        //    __('Search & replace', 'mgmsr'),
        //    __('Search & replace', 'mgmsr'),
        //    'manage_options',
        //    sprintf('%s-sr', MGMSR_PLUGIN_ABBR),
        //    array($this, 'display_sr_page')
        //);
    }

    public function nav()
    {
        global $self, $parent_file, $submenu_file, $plugin_page, $typenow, $submenu;
        $submenus = array();

        $menuItem = MGMSR_ADMINMENU_SLUG;

        if (isset($submenu[$menuItem])) {
            $thisMenu = $submenu[$menuItem];

            foreach ($thisMenu as $sub_item) {
                $slug = $sub_item[2];

                if ($slug == MGMSR_PLUGIN_ABBR . "-pdf-template") {
                    continue;
                }

                // Handle current for post_type=post|page|foo pages, which won't match $self.
                $self_type = !empty($typenow) ? $self . '?post_type=' . $typenow : 'nothing';

                $isCurrent = false;
                $subpageUrl = get_admin_url('', 'admin.php?page=' . $slug);

                if ((!isset($plugin_page) && $self == $slug) || (isset($plugin_page) && $plugin_page == $slug && ($menuItem == $self_type || $menuItem == $self || file_exists($menuItem) === false))) {
                    $isCurrent = true;
                }

                $url = (strpos($slug, '.php') !== false || strpos($slug, 'http') !== false) ? $slug : $subpageUrl;
                $isExternalPage = strpos($slug, 'http') !== false;
                $submenus[] = array(
                    'link' => $url,
                    'title' => $sub_item[0],
                    'current' => $isCurrent,
                    'target' => $isExternalPage ? '_blank' : '',
                );
            }
        }
        return Templates_helper::load(MGMSR_ADMIN_TEMPLATES_DIR . 'nav.php', array('submenus' => $submenus));
    }

    public function admin_menu_content()
    {
        $content = Templates_helper::load(MGMSR_ADMIN_TEMPLATES_DIR . 'menu/mgmsr.php');
        echo Templates_helper::load(MGMSR_ADMIN_TEMPLATES_DIR . 'template.php', array(
            'nav' => $this->nav(),
            'content' => $content)
        );
    }

    public function display_options_page()
    {
        $content = Templates_helper::load(MGMSR_ADMIN_TEMPLATES_DIR . 'menu/options.php');
        echo Templates_helper::load(MGMSR_ADMIN_TEMPLATES_DIR . 'template.php', array(
            //'nav' => $this->nav(),
            'content' => $content)
        );
    }

    public function display_sr_page()
    {
        $content = Templates_helper::load(MGMSR_ADMIN_TEMPLATES_DIR . 'menu/search-replace.php');
        echo Templates_helper::load(MGMSR_ADMIN_TEMPLATES_DIR . 'template.php', array(
            //'nav' => $this->nav(),
            'content' => $content)
        );
    }

    /**
     * Search & Show
     * Search & Replace
     * Display results table
     */
    public function display_results_table()
    {
        try {
            $search = !empty($_POST["search"]) ? sanitize_text_field($_POST["search"]) : Options::get_option('search');
            $replace = !empty($_POST["replace"]) ? sanitize_text_field($_POST["replace"]) : Options::get_option('replace');
            $where = !empty($_POST["where"]) ? $_POST["where"] : Options::get_option('where');
            $cron = !empty($_POST["cron"]) ? $_POST["cron"] : Options::get_option('cron');

            // Search & Show
            if (!empty($_POST["mgmsr_search_show_btn"]) && $_POST["mgmsr_search_show_btn"] == 1) {
                // Search
                $results = Results::get_items(array(
                    "search" => $search,
                    "replace" => $replace,
                    "where" => $where,
                ));
                if ($results["errors"]) {
                    throw new \Exception(implode(";", $results["errors"]));
                }
                if (!$results["items"]) {
                    throw new \Exception(__("No items found", "mgmsr"));
                }
                // Display results
                Results::display_results($results);
            }

            // Search & Replace
            if (!empty($_POST["mgmsr_search_replace_btn"]) && $_POST["mgmsr_search_replace_btn"] == 1) {
                // Search
                $results = Results::get_items(array(
                    "search" => $search,
                    "replace" => $replace,
                    "where" => $where,
                ));
                if ($results["errors"]) {
                    throw new \Exception(implode(";", $results["errors"]));
                }
                if (!$results["items"]) {
                    throw new \Exception(__("No items found", "mgmsr"));
                }
                // Replace
                $results = Results::replace($results);
                if ($results["errors"]) {
                    throw new \Exception(implode(";", $results["errors"]));
                }
                // Display results
                $results = Results::display_results($results);
                if ($results["errors"]) {
                    throw new \Exception(implode(";", $results["errors"]));
                }
                jsalert(__("Replaced successfully!", "mgmsr"));
            }

        } catch (\Throwable $e) {
            jsalert($e->getMessage());
        }
    }
	
}
