<?php

namespace mgmsr\lib;

use mgmsr\lib\admin\List_table;
use mgmsr\lib\helpers\Templates_helper;

class Results {
	
	static function get_fields(){
		$fields = array(
			"search" => array(
				"code" => "search",
				"title" => __("Search for", "mgmsr"),
				"show_in_admin" => true,
				"default_value" => ""
			),
			"replace" => array(
				"code" => "replace",
				"title" => __("Replace to", "mgmsr"),
				"show_in_admin" => true,
				"default_value" => ""
			),
			"found" => array(
				"code" => "found",
				"title" => __("Found", "mgmsr"), // may be "blogs", "pages"
				"show_in_admin" => true,
				"default_value" => ""
			),
			"post_id" => array(
				"code" => "post_id",
				"title" => __("Found post id", "mgmsr"), // may be "blogs", "pages"
				"show_in_admin" => false,
				"default_value" => ""
			),
		);
		return $fields;
	}
	
	public static function get_list_table_columns(){
		$fields = static::get_fields();
		$columns = array();
		foreach ($fields as $key => $field) {
			if($field["show_in_admin"]){
				$columns[$field["code"]] = $field;
			}
		}
		return $columns;
	}
	
	/**
	 * @return array with items or errors
	 */
	public static function get_items($args){
		try{
			$results = array("items" => array(), "errors" => array());
			$defaults = array(
				"search" => "",
				"replace" => "",
				"where" => array(),
			);
			$args = wp_parse_args( $args, $defaults );
			//ppr($args, __FILE__.'$args');
			
			if(!$args["search"]){
				throw new \Exception(__("Search parameter not defined", "mgmsr"));
			}
			if(!$args["where"]){
				throw new \Exception(__("Where parameter not defined", "mgmsr"));
			}
			if(mb_strlen($args["search"]) < 2){
				throw new \Exception(__("Length of Search parameter should be more then 1", "mgmsr"));
			}
			if(mb_strlen($args["replace"]) < 2){
				throw new \Exception(__("Length of Replace parameter should be more then 1", "mgmsr"));
			}
			
			/* all ok! */
			
			// generate sql query
			global $wpdb;
			$sql_select = "`ID`, `post_title`";
			
			$sql_where = "WHERE (`post_status`='publish')";
			$post_types = array();
			foreach ( $args["where"] as $post_type ) {
				$post_types[] = sprintf("`post_type` = '%s'", $post_type);
			}
			$sql_where .= sprintf(" AND (%s)", implode(" OR ", $post_types));
			$sql_where .= " AND (`post_content` LIKE BINARY '%" . $args["search"] . "%')"; // BINARY to case sensitive searching
			$sql_sort = "ORDER BY `post_title` ASC";
			
			$sql_query = sprintf('SELECT %1$s FROM `%2$s` %3$s %4$s', $sql_select, $wpdb->posts, $sql_where, $sql_sort );
			
			$_posts = $wpdb->get_results($sql_query, ARRAY_A);
			//ppr($wpdb->last_query, __FILE__.' $wpdb->last_query');
			
			foreach ( $_posts as $_post ) {
				$results["items"][] = array(
					"search" => $args["search"],
					"replace" => $args["replace"],
					"found" => sprintf('<a href="%s" target="_blank">%s</a>', get_permalink($_post["ID"]), $_post["post_title"]),
					"post_id" => $_post["ID"],
				);
			}
		} catch (\Throwable $e) {
			$results["errors"][] = $e->getMessage();
		} finally {
			return $results;
		}
	}
	
	
	/**
	 * Replacing
	 * @return array with items or errors
	 */
	public static function replace($results){
		try{
			$defaults = array("items" => array(), "errors" => array());
			$results = wp_parse_args( $results, $defaults );
			
			if($results["errors"]){
				throw new \Exception(implode(";", $results["errors"]));
			}
			if(!$results["items"]){
				throw new \Exception(__("No items found", "mgmsr"));
			}
			
			/* all ok! */
			$update_errors = array();
			foreach ( $results["items"] as $item ) {
				$_post = get_post($item["post_id"]);
				$raw_content = $_post->post_content;
				//fppr($raw_content, __FILE__.'$raw_content');
				
				$pattern = preg_quote ($item["search"], '/');
				$raw_new_content = preg_replace('/' . $pattern . '/', $item["replace"], $raw_content);
				//fppr($raw_new_content, __FILE__.'$raw_new_content');
				
				$new_post_data = array(
					'ID' => $item["post_id"],
					'post_content' => $raw_new_content,
				);
				
				// Update the post
				$res = wp_update_post( wp_slash($new_post_data));
				if(is_wp_error( $res )){
					$update_errors[] = $res->get_error_message();
				}
				if($res === 0){
					$update_errors[] = __("Error update post", "mgmsr") . "(" . $item["post_id"] . ")";
				}
			}
			if($update_errors){
				throw new \Exception(__("Updated with errors: ", "mgmsr") . implode("; ", $update_errors));
			}
			
		} catch (\Throwable $e) {
			$results["errors"][] = $e->getMessage();
		} finally {
			return $results;
		}
	}
	
	public static function display_results($results) {
		try {
			if ( $results["errors"] ) {
				throw new \Exception( implode( ";", $results["errors"] ) );
			}
			if ( ! $results["items"] ) {
				throw new \Exception( __( "No items found", "mgmsr" ) );
			}
			$columns       = self::get_list_table_columns();
			$column_titles = wp_list_pluck( $columns, 'title' );
			
			// sorting
			//$sortable_columns = array(
			//	'found' => array('found', false),
			//);
			//// default sorting
			//if(empty($_GET['orderby'])){
			//	$_GET['orderby'] = 'found';
			//}
			//if(empty($_GET['order'])){
			//	$_GET['order'] = 'asc';
			//}
			
			// prepare results table
			$items              = $results["items"];
			$results_list_table = new List_table();
			$results_list_table->set_columns( $column_titles );
			//$results_list_table->set_sortable_columns($sortable_columns);
			$results_list_table->set_table_data( $items );
			$results_list_table->set_limit( 10000 );
			$results_list_table->prepare_items();
			// show results table
			echo Templates_helper::load( MGMSR_ADMIN_TEMPLATES_DIR . 'table-results.php', array(
				'results_list_table' => $results_list_table,
			) );
		} catch ( \Throwable $e ) {
			$results["errors"][] = $e->getMessage();
		} finally {
			return $results;
		}
	}
	
}